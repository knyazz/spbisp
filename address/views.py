#coding: utf-8
from django.db.models import Q
from django.views.generic import FormView

from rest_framework import generics

from .forms import UpdateAddressForm
from .models import Address, Street
from .serializers import AddressSerializer
from .tasks import async_update_addresses


class StreetNameList(generics.ListAPIView):
    model = Street

    def filter_queryset(self, queryset):
        qs = super(StreetNameList, self).filter_queryset(queryset)
        #TODO: refact this
        q = Q()
        if self.request.GET:
            body = self.request.GET
            if body.get('title'):
                k = body.get('title')
                kq = Q(title__icontains=k)
                q&= kq
        return qs.filter(q)
street_names = StreetNameList.as_view()


class Addresses(generics.ListAPIView):
    model = Address
    serializer_class = AddressSerializer
    queryset = model.objects.filter(region__title='Санкт-петербург')

    def filter_queryset(self, queryset):
        qs = super(Addresses, self).filter_queryset(queryset)
        #TODO: refact this
        q=Q()
        if self.request.GET:
            body = self.request.GET
            if body.get('street_pk'):
                q&=Q(street__pk=body.get('street_pk'))
            if body.get('house'):
                q&=Q(house=body.get('house'))
            elif body.get('houses'):
                q&=Q(house__startswith=body.get('houses'))
            if body.get('housing'):
                q&=Q(housing=body.get('housing'))
            elif body.get('search'):
                q&=(Q(housing__isnull=True)|Q(housing=''))
            if body.get('prvdr'):
                q&=Q(providers=body.get('prvdr'))
        return qs.filter(q)
addresses=Addresses.as_view()


class UpdateAddressView(FormView):
    form_class = UpdateAddressForm
    template_name = 'admin/update_addresses.html'

    def get_success_url(self):
        return Address.admin_list_link()
        #return self.request.path

    def form_valid(self, form):
        fl = form.files.get('csvfile')
        prvdr = form.cleaned_data.get('provider').title
        async_update_addresses.delay(fl, prvdr)
        return super(UpdateAddressView, self).form_valid(form)
update_addresses = UpdateAddressView.as_view()