#coding: utf-8
from __future__ import unicode_literals

from spbisp.celery import app

from .utils import update_address_from_csv_file


@app.task(ignore_result=True)
def async_update_addresses(fl, prvdr):
    b'''
        Основная celery функция обновления базы адресов.
    '''
    update_address_from_csv_file(fl, prvdr)