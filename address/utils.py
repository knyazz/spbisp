# -*- coding: utf-8 -*-
import csv


from address.models import Address, City, District, Region, Street
from providers.models import ITStandart, Provider, Service


DEFAULT_REGION = 'Санкт-петербург'

def update_address_from_csv_file(csvfile, provider=None):
    u'''
        район, нас. пункт,Префикс нас. пункта,улица,Префикс улицы,Номер дома,Корпус дома,Интернет,ЦФ ТВ,ТЛФ
        город/деревня,район,улица,дом,корпус,Интернет,ЦФ ТВ(Интерактивное),ТЛФ,Технология
    '''
    data = csv.reader(csvfile)
    write=False
    addresses = list()
    for row in data:
        if write:
            dt = dict()
            name = ' '.join(row[3:5]).decode('utf-8').lower().capitalize()
            dt['street'], _crt = Street.objects.get_or_create(title=name)
            dt['district'], _crt = District.objects.get_or_create(title=row[0].decode('utf-8'
                                                                        ).lower(
                                                                        ).capitalize()
                                                                )
            dt['region'], _crt = Region.objects.get_or_create(title=DEFAULT_REGION)
            if row[1]:
                name = row[1].decode('utf-8').lower().capitalize()
                dt['city'], _crt = City.objects.get_or_create(title=name)
            else:
                dt['city'], _crt = City.objects.get_or_create(title=DEFAULT_REGION)
            dt['house'] = row[5].decode('utf-8').capitalize()
            dt['housing'] = row[6].decode('utf-8').capitalize()
            addr, _crt = Address.objects.get_or_create(**dt)
            addresses.append(addr.pk)
            for rw,name in {
                            row[7]: 'Интернет',
                            row[8]: 'Телевидение',
                            row[9]: 'Телефония',
            }.items():
                if rw:
                    serv, _crt = Service.objects.get_or_create(title=name)
            if len(row) > 10 and row[10]:
                st, _crt = ITStandart.objects.get_or_create(title=row[10])
                addr.standarts.add(st)
            provider = provider if provider else 'Ростелеком'
            rt, _crt = Provider.objects.get_or_create(title=provider)
            addr.providers.add(rt)
        write=True
        # удаляем провайдера из адресов, которых нет в файле
        addresses = Address.objects.filter(providers=rt
                                  ).exclude(pk__in=set(addresses))
        for addrs in addresses:
            if addrs.providers.count() > 1:
                addrs.providers.remove(rt)
            else:
                addrs.delete()
    return 1