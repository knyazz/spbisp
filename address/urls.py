from django.conf.urls import patterns, url


urlpatterns = patterns('address.views',
    url(r'^addresses/$', 'addresses', name='addresses'),
    url(r'^street_names/$', 'street_names', name='street_names'),
)
