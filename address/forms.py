#coding: utf-8
from django import forms

from providers.models import Provider

class UpdateAddressForm(forms.Form):
    csvfile = forms.FileField(label=u'Выберите csv-файл')
    provider = forms.ModelChoiceField(queryset=Provider.objects.all())