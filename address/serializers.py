#coding: utf-8
from rest_framework import serializers

from providers.serializers import ProviderSerializer

from .models import Address, City, District, Street


class StreetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Street
        fields = 'id', 'title'


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = 'id', 'title'


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = 'id', 'title'


class AddressSerializer(serializers.ModelSerializer):
    full_name = serializers.Field(source='full_address')
    providers = ProviderSerializer()
    class Meta:
        model = Address
        fields = (  'id', 'full_name', 'providers', 'house', 'housing',)