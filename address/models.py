#coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models

from base.models import BaseModel
from providers.models import Provider, ITStandart


class District(BaseModel):
    class Meta:
        verbose_name=u'Район'
        verbose_name_plural=u'Районы'


class Region(BaseModel):
    class Meta:
        verbose_name=u'Регион'
        verbose_name_plural=u'Регион'


class City(BaseModel):
    class Meta:
        verbose_name=u'Город'
        verbose_name_plural=u'Города'


class Street(BaseModel):
    class Meta:
        verbose_name=u'Улица'
        verbose_name_plural=u'Улицы'


class Address(models.Model):
    region = models.ForeignKey(Region, verbose_name=Region._meta.verbose_name,
                                null=True)
    city = models.ForeignKey(City, verbose_name=City._meta.verbose_name)
    district = models.ForeignKey(District, null=True, blank=True,
                                verbose_name=District._meta.verbose_name)
    street = models.ForeignKey(Street,verbose_name=Street._meta.verbose_name)
    house = models.CharField(u'Дом', max_length=8)
    housing = models.CharField(u'Корпус', max_length=8, blank=True)
    providers = models.ManyToManyField(Provider,
                                verbose_name=Provider._meta.verbose_name_plural)
    standarts = models.ManyToManyField(ITStandart, null=True, blank=True,
                                verbose_name=ITStandart._meta.verbose_name_plural)

    class Meta:
        verbose_name=u'Адрес'
        verbose_name_plural=u'Адреса'

    @property
    def full_address(self):
        lst = (self.city.title, self.street.title, self.house, self.housing)
        return u', '.join(lst)

    __unicode__ = lambda self: self.full_address

    @property
    def admin_link(self):
        if self.pk:
            return reverse('admin:address_address_change', args=(self.pk,),)

    @classmethod
    def admin_list_link(cls):
        return reverse('admin:address_address_changelist')