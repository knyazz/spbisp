#coding: utf-8
from django.contrib import admin

from base.admin import BaseAdmin

from .models import Address, City, District, Street, Region

class AddressAdmin(BaseAdmin):
    list_filter = ('region', 'city', 'street', 'house')
admin.site.register(Address,AddressAdmin)
admin.site.register(City,BaseAdmin)
admin.site.register(District,BaseAdmin)
admin.site.register(Street,BaseAdmin)
admin.site.register(Region,BaseAdmin)