# -*- coding: utf-8 -*-
import os

from django.conf import settings
from django.core.management import BaseCommand

from address.utils import update_address_from_csv_file


def cp1251_decoder(data):
    for line in data:
        yield line.decode('cp1251').encode('utf-8')

class Command(BaseCommand):
    u'''
        город/деревня,район,улица,дом,корпус,Интернет,ЦФ ТВ(Интерактивное),ТЛФ,Технология
    '''
    help = 'Use: ./manage.py update_address_from_csv'

    def handle(self, *args, **options):
        dr = os.path.join(settings.BASE_DIR, 'spbisp/fixtures/')
        flname = args[0]
        provider = args[1]
        with open(dr+flname, 'rb') as csvfile:
            update_address_from_csv_file(csvfile, provider)
        return 'done'