from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^captcha/', include('captcha.urls')),
    (r'^ckeditor/', include('ckeditor.urls')),

    url(r'^address/', include('address.urls', namespace='address')),
    url(r'^admin/address/update_from_csv/',
        'address.views.update_addresses',
        name='update_addresses'
    ),
    url(r'^base/', include('base.urls', namespace='base')),
    url(r'^orders/', include('orders.urls', namespace='orders')),
    url(r'^providers/', include('providers.urls', namespace='providers')),
    url(r'^reviews/', include('reviews.urls', namespace='reviews')),
)

urlpatterns+= patterns('base.views',
    url(r'^$', 'index', name='index'),
    url(r'^order/$', 'order', name='order'),
)

if settings.DEBUG:
    urlpatterns = patterns('',
        url(r'', include('django.contrib.staticfiles.urls')),
    ) + urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)