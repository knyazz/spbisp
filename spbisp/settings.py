#coding: utf-8
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
SECRET_KEY = '1vh$y(g6e9v&q5x21gst_4h4qzqs$x(5(m*m@zlcf0%&s^)=za'

DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'captcha',
    'ckeditor',
    'django_select2',
    'filer',
    'easy_thumbnails',
    'south',
    'rest_framework',
    'widget_tweaks',

    'address',
    'base',
    'orders',
    'providers',
    'reviews',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',

    'base.context_processors.content',
    'orders.context_processors.content',
)
if DEBUG:
    TEMPLATE_CONTEXT_PROCESSORS+= ('django.core.context_processors.debug',)

ROOT_URLCONF = 'spbisp.urls'

WSGI_APPLICATION = 'spbisp.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

LANGUAGE_CODE = 'ru'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
CKEDITOR_UPLOAD_PATH = 'filer_public/'
FILEBROWSER_DIRECTORY = CKEDITOR_UPLOAD_PATH

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)

SUIT_CONFIG = {
    'ADMIN_NAME': u'спб-интернет.рф',
    'CONFIRM_UNSAVED_CHANGES': True,
    'MENU_EXCLUDE': ('auth', 'sites'),
    'MENU': (
        {'label': u'Обновить Адреса из файла', 'url': 'update_addresses'},
        {'app': 'address', 'label': u'Адреса',},
        {'app': 'base', 'label': u'Общее',},        
        {'app': 'filer', 'label': u'Файлы',},
        {'app': 'orders', 'label': u'Заявки',},
        {'app': 'providers', 'label': u'Провайдеры',},
        {'app': 'reviews', 'label': u'Отзывы',},
    )
}

CKEDITOR_TOOLBAR = (
    [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ],
    [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','aRedo' ],
    [ 'Find','Replace','-','SelectAll','-','SpellChecker'],
    [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ],
    [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidaiRtl' ],
    [ 'Link','Unlink','Anachor' ],
    [ 'Image', 'Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBareak' ],
    [ 'Styles','Format','Font','FontaSize' ],
    [ 'TextColor','BGColor' ],
    [ 'Maximize', 'ShowBlocks'],
)

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': CKEDITOR_TOOLBAR,
        'height': 400,
        'width': 'auto',
        'disableNativeSpellChecker': False,
        'forcePasteAsPlainText': True,
        'startupOutlineBlocks': True,
    },
}

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = ['--with-notify',
             '--no-start-message',
             '--verbosity=2',
             '--with-fixture-bundling',
             ]

SOUTH_MIGRATION_MODULES = {
    'easy_thumbnails': 'easy_thumbnails.south_migrations',
}

CELERY_ACCEPT_CONTENT = ('pickle', 'json', 'msgpack', 'yaml')
BROKER_URL = 'amqp://guest:guest@localhost:5672//'

try:
    from local_settings import *
except ImportError:
    pass