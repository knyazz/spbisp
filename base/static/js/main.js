(function($) {
	$(function() {
		$('#partners').owlCarousel({
			navigation : true, // Show next and prev buttons
			slideSpeed : 300,
			paginationSpeed : 400,
			pagination:false,
			singleItem:true,
			stopOnHover:true,
			navigationText:["<a class='prevSlide'>&lt;</a>","<a class='nextSlide'>&gt;</a>"]
			});

		var currentRate = $('.ratingField input:checked').val();
		var obRate = $('#rating');
		$('.ratingField input').change(function(){
				changeRating(obRate,$(this).val());
			});
		$('.ratingField input').hover(function(){
				$(this).trigger('click');
			});
		$('.providerDescriptionsTab').slideUp('0');
		$("a[href='#about']").click(function(e){
			e.preventDefault();
			if($(this).hasClass('active')){
				$(this).removeClass();
				$(this).parent().parent().find('.providerDescription').slideUp('100');
				}
			else{
				$(this).siblings('a').removeClass();
				$(this).addClass('active');
				$(this).parent().parent().find('.providerDescriptionsTab').slideUp('100');
				$(this).parent().parent().find('.providerDescription').slideDown('100');
				}
			});
		$("a[href='#actions']").click(function(e){
			e.preventDefault();
			if($(this).hasClass('active')){
				$(this).removeClass();
				$(this).parent().parent().find('.providerActions').slideUp('100');
				}
			else{
				$(this).siblings('a').removeClass();
				$(this).addClass('active');
				$(this).parent().parent().find('.providerDescriptionsTab').slideUp('100');
				$(this).parent().parent().find('.providerActions').slideDown('100');
				}
			});
		$("a[href='#tarifs']").click(function(e){
			e.preventDefault();
			if($(this).hasClass('active')){
				$(this).removeClass();
				$(this).parent().parent().find('.providerTarifes').slideUp('100');
				}
			else{
				$(this).siblings('a').removeClass();
				$(this).addClass('active');
				$(this).parent().parent().find('.providerDescriptionsTab').slideUp('100');
				$(this).parent().parent().find('.providerTarifes').slideDown('100');
				}
			});
		$(document).ready(function(){
			if(window.location.pathname!="/"){
				$('.searchBar').ScrollTo({duration: 1200});
				}
			});
		$('.searchBtn').click(function(){
			$('.providersList').ScrollTo({duration: 500});
			$('.providerResumeLinks a').removeClass();
			$('.providerDescriptionsTab').slideUp('100');
			});
	});
})(jQuery);

function changeRating(obRating,ratingValue){
	obRating.removeClass();
	obRating.addClass('rating'+ratingValue);
	}

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}