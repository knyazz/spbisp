$(function () {
    var AppState = Backbone.Model.extend({
        defaults: {
            AddressTitle: "",
            state: "start"
        }
    });

    var appState = new AppState();

    var AddressTitleModel = Backbone.Model.extend({ // Модель адресов
        defaults: {
            "Title": ""
        }
    });

    var Addresses = Backbone.Collection.extend({ // Коллекция адресов

        model: AddressTitleModel,

        checkAddress: function (AddressTitle) { // Проверка адреса
            var findResult = this.find(function (address) { return address.get("Title") == AddressTitle })
            return findResult != null;
        }

    });

    var MyAddresses = new Addresses([ // Моя семья
                { Title: "Саша" },
                { Title: "Юля" },
                { Title: "Елизар" },

            ]);



    var Controller = Backbone.Router.extend({
        routes: {
            "": "start", // Пустой hash-тэг
            "!/": "start", // Начальная страница
            "!/success": "success", // Блок удачи
            "!/error": "error" // Блок ошибки
        },

        start: function () {
            appState.set({ state: "start" });
        },

        success: function () {
            appState.set({ state: "success" });
        },

        error: function () {
            appState.set({ state: "error" });
        }
    });

    var controller = new Controller(); // Создаём контроллер


    var Block = Backbone.View.extend({
        el: $("#providers"), // DOM элемент widget'а

        templates: { // Шаблоны на разное состояние
            "start": _.template($('#start').html()),
            "success": _.template($('#success').html()),
            "error": _.template($('#error').html())
        },

        events: {
            "click button.searchBtn": "check" // Обработчик клика на кнопке "Проверить"
        },

        initialize: function () { // Подписка на событие модели
            this.model.bind('change', this.render, this);
        },

        check: function () {
            var el = $(".searchForm");
            var address = el.find("input:text").val();
            var find = MyAddresses.checkAddress(address); // Проверка адреса
            appState.set({ // Сохранение адреса и состояния
                "state": find ? "success" : "error",
                "AddressTitle": address
            });
        },

        render: function () {
            var state = this.model.get("state");
            $(this.el).html(this.templates[state](this.model.toJSON()));
            return this;
        }
    });

    var block = new Block({ model: appState }); // создадим объект

    appState.trigger("change"); // Вызовем событие change у модели

    appState.bind("change:state", function () { // подписка на смену состояния для контроллера
        var state = this.get("state");
        if (state == "start")
            controller.navigate("!/", false); // false потому, что нам не надо 
                                              // вызывать обработчик у Router
        else
            controller.navigate("!/" + state, false);
    });

    Backbone.history.start();  // Запускаем HTML5 History push
});