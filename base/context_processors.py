#coding: utf-8
from .models import SliderItem, BlockOnSlider, SystemTunes


def slice_list(qs=None, count=3):
    if qs and qs.count()>count:
        return qs[:count]
    return qs

def content(request):
    slider_qs = SliderItem.objects.filter(is_active=True)
    return dict(
        left_block = BlockOnSlider.objects.filter(tp=0).last(),
        center_block = BlockOnSlider.objects.filter(tp=1).last(),
        right_block = BlockOnSlider.objects.filter(tp=2).last(),
        slider = slice_list(slider_qs, 5),
        systemtune = SystemTunes.objects.filter(is_active=True).last(),
    )