#coding: utf-8
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.views.generic import DetailView
from django.views.generic import TemplateView

from address.models import Address
from address.views import Addresses
from providers.models import Provider

from .models import FlatPage


class IndexPage(TemplateView):
    template_name='index.html'

    def get_context_data(self, **kwargs):
        ctx = super(IndexPage, self).get_context_data(**kwargs)
        ctx['providers'] = Provider.objects.all()[:3]
        if self.request.GET:
            ctx['results'] = self.get_addrs()
        return ctx

    def get_addrs(self):
        qs = Address.objects.all()
        qs = Addresses(request=self.request).filter_queryset(qs)
        return qs
index = IndexPage.as_view()


class DetailPage(DetailView):
    template_name = 'object_detail.html'
    context_object_name = 'detail_page'
    model = FlatPage

    def get(self, request, *args, **kwargs):
        if self.get_object():
            return super(DetailPage, self).get(request, *args, **kwargs)
        else:
            return redirect(reverse('index'))
flat_page = DetailPage.as_view()


class OrderPage(TemplateView):
    template_name='order.html'
order = OrderPage.as_view()