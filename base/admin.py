#coding: utf-8
from django import forms
from django.contrib import admin
from django.db import models

from django_select2.widgets import Select2MultipleWidget, Select2Widget
from suit.admin import SortableModelAdmin
from suit.widgets import LinkedSelect, SuitDateWidget, SuitSplitDateTimeWidget

from .models import FlatPage, SliderItem, BlockOnSlider, SystemTunes

class LinkedSelect2(Select2Widget, LinkedSelect):
    minimumResultsForSearch = 10
    closeOnSelect = True


class BaseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        for k,v in self.fields.items():
            #if self.fields[k].required:
                #v.widget.attrs['required']='required'
            if k == 'title':
                v.widget.attrs['style']='width: 100%;'


class BaseAdmin(admin.ModelAdmin):
    form = BaseForm
    formfield_overrides = {
        models.DateField: {'widget': SuitDateWidget},
        models.DateTimeField: {'widget': SuitSplitDateTimeWidget},
        models.ForeignKey: {'widget': LinkedSelect2(select2_options={'width': 'resolve'})},
        models.OneToOneField: {'widget': LinkedSelect2(select2_options={'width': 'resolve'})},
        models.ManyToManyField: {'widget': Select2MultipleWidget(select2_options={'width': 'resolve'})},
    }
    class Media:
        js = (
                'js/admin.js',
        )
admin.site.register(SliderItem, BaseAdmin)
admin.site.register(BlockOnSlider, BaseAdmin)


class BaseAdminwithOrder(SortableModelAdmin, BaseAdmin):
    sortable='sorting'


class FlatPageAdmin(BaseAdmin):
    prepopulated_fields = { 
                            'slug': ('title',)
                        }
admin.site.register(FlatPage, FlatPageAdmin)


class SystemTunesAdmin(BaseAdmin):
    fields = ('title', 'google_analytics', 'yandex_metrics', 'is_active')
admin.site.register(SystemTunes, SystemTunesAdmin)