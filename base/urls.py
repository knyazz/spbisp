from django.conf.urls import patterns, url

urlpatterns = patterns('base.views',
    url(r'^flatpages/(?P<slug>.*)/$', 'flat_page', name='flat_page'),
)