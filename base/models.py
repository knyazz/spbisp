#coding: utf-8
from django.db import models
from django.core.urlresolvers import reverse

from easy_thumbnails.files import get_thumbnailer
from filer.fields.image import FilerImageField

from ckeditor.fields import RichTextField


class BaseModel(models.Model):
    title = models.CharField(u'Название', max_length=255)
    __unicode__ = lambda self: self.title
    class Meta:
        abstract=True


class FlatPage(BaseModel):
    content = RichTextField(u'Содержание')
    slug = models.SlugField(u'Путь', max_length=255, unique=True,
                help_text=u'Заполнить название'
    )

    class Meta:
        verbose_name= u'простая страница'
        verbose_name_plural = u'простые страницы'

    def get_absolute_url(self):
        return reverse('base:flat_page', kwargs=dict(slug=self.slug))


class SliderItem(models.Model):
    flr_image = FilerImageField(verbose_name=u'Изображение')
    is_active = models.BooleanField(u'Показать на главной', default=True)

    @property
    def image_hd_url(self):
        opts = dict(size=(1920,1080))
        thumbnail = get_thumbnailer(self.flr_image).get_thumbnail(opts)
        return thumbnail.url

    class Meta:
        verbose_name= u'Элемент слайдера'
        verbose_name_plural = u'Элементы слайдера'


class BlockOnSlider(models.Model):
    title = models.CharField(u'Название', max_length=44)
    text = models.TextField(u'Teкст', max_length=185)
    tp = models.PositiveSmallIntegerField(u'Расположение блока', unique=True,
                            choices=(
                                        (0, u'Левый'),
                                        (1, u'Центр'),
                                        (2, u'Правый'),
                            )
    )
    __unicode__ = lambda self: self.title

    class Meta:
        verbose_name= u'Блок на слайдере'
        verbose_name_plural = u'Блоки на слайдере'


class SystemTunes(BaseModel):
    phone = models.CharField(u'Телефон', max_length=255, blank=True)
    email = models.EmailField('email', blank=True)
    is_active = models.BooleanField(u'Активный', default=True)
    sliderspeed = models.PositiveSmallIntegerField(u'Скорость слайдера, сек',
        default=10)
    google_analytics = models.TextField(u'Код Google-аналитики', blank=True)
    yandex_metrics = models.TextField(u'Код Yandex-метрики', blank=True)
    keywords = models.CharField(u'SEO keywords',
                                max_length=2048, blank=True)
    description = models.CharField(u'SEO description',
                                max_length=2048, blank=True)
    class Meta:
        verbose_name= u'Системные настройки'
        verbose_name_plural = verbose_name

    sliderspeedms = property(lambda self: self.sliderspeed*1000)