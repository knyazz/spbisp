from django.conf.urls import patterns, url

urlpatterns = patterns('providers.views',
    url(r'^prvdrs_list/$', 'prvdrs_list', name='prvdrs_list'),
    url(r'^specials/$', 'specials', name='specials'),
)