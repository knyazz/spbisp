# coding: utf-8
import datetime

from django.db import models


class ProviderQuerySet(models.query.QuerySet):
    def prvdrs_with_actual_specials(self):
        now = datetime.datetime.now()
        q = (
            models.Q(specials__isnull=False) &
            (   models.Q(specials__start_date__isnull=True) & 
                models.Q(specials__start_date__isnull=True)
            ) |
            (
                models.Q(specials__start_date__isnull=False) &
                models.Q(specials__start_date__lte=now) &
                models.Q(specials__end_date__isnull=False) &
                models.Q(specials__end_date__gte=now)
            )
        )
        ids = self.filter(q).values_list('id', flat=True)
        return self.filter(id__in=set(ids))


class ProviderManager(models.Manager):
    def get_queryset(self):
        return ProviderQuerySet(self.model, using=self._db)

    def prvdrs_with_actual_specials(self):
        return self.get_queryset().prvdrs_with_actual_specials()


class SpecialsQuerySet(models.query.QuerySet):
    def actual_specials(self):
        now = datetime.datetime.now()
        q = (
            (   models.Q(start_date__isnull=True) & 
                models.Q(start_date__isnull=True)
            ) |
            (
                models.Q(start_date__isnull=False) &
                models.Q(start_date__lte=now) &
                models.Q(end_date__isnull=False) &
                models.Q(end_date__gte=now)
            )
        )
        return self.filter(q)


class SpecialsManager(models.Manager):
    def get_queryset(self):
        return SpecialsQuerySet(self.model, using=self._db)

    def actual_specials(self):
        return self.get_queryset().actual_specials()


class TariffQuerySet(models.query.QuerySet):
    def internet(self):
        return self.exclude(services__in=(2,3))

    def internet_tv(self):
        return self.filter(services=2).exclude(services__in=(3,))

    def internet_phone_tv(self):
        return self.filter(services=3)


class TariffManager(models.Manager):
    def get_queryset(self):
        return TariffQuerySet(self.model, using=self._db)

    def internet(self):
        return self.get_queryset().internet()

    def internet_tv(self):
        return self.get_queryset().internet_tv()

    def internet_phone_tv(self):
        return self.get_queryset().internet_phone_tv()