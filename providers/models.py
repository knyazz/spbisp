#coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models

from ckeditor.fields import RichTextField
from easy_thumbnails.files import get_thumbnailer
from filer.fields.image import FilerImageField

from base.models import BaseModel

from .choices import SERVICETYPES
from .managers import ProviderManager, SpecialsManager, TariffManager


class ITStandart(BaseModel):
    sorting = models.PositiveIntegerField('Сортировка', default=0)
    class Meta:
        ordering= 'sorting', 'pk',
        verbose_name=u'Стандарт передачи данных'
        verbose_name_plural=u'Стандарты передачи данных'


class Service(BaseModel):
    sorting = models.PositiveIntegerField('Сортировка', default=0)
    tp = models.CharField(u'Тип услуги', default='internet_home.png',
                        choices=SERVICETYPES, max_length=32)
    class Meta:
        ordering= 'sorting', 'pk',
        verbose_name=u'Услуга'
        verbose_name_plural=u'Услуги'


class Tariff(BaseModel):
    objects = TariffManager()
    services = models.ManyToManyField(Service, null=True, blank=True,
                                verbose_name=Service._meta.verbose_name_plural)
    standarts = models.ManyToManyField(ITStandart, null=True, blank=True,
                                verbose_name=ITStandart._meta.verbose_name_plural)
    desc = RichTextField(u'Описание', max_length=4096)
    price = models.CharField(u'Стоимость', max_length=255)
    speed = models.CharField(u'Скорость', blank=True, null=True, max_length=255)
    con_term = models.CharField(u'Срок подключения, дней', blank=True, null=True,
                                max_length=255)
    sorting = models.PositiveIntegerField('Сортировка', default=0)
    class Meta:
        ordering= 'sorting', 'pk',
        verbose_name=u'Тариф'
        verbose_name_plural=u'Тарифы'


class Special(BaseModel):
    objects = SpecialsManager()
    services = models.ManyToManyField(Service, null=True, blank=True,
                                verbose_name=Service._meta.verbose_name_plural)
    desc = RichTextField(u'Описание', max_length=4096)
    start_date = models.DateTimeField(u'Начало', null=True)
    end_date = models.DateTimeField(u'Окончание', null=True)
    sorting = models.PositiveIntegerField('Сортировка', default=0)
    tariffs = models.ForeignKey(Tariff, null=True, blank=True,
                                verbose_name=Tariff._meta.verbose_name)
    class Meta:
        ordering= 'sorting', 'pk',
        verbose_name=u'Акция'
        verbose_name_plural=u'Акции'


class Provider(BaseModel):
    objects= ProviderManager()
    site = models.URLField(u'Cайт', blank=True)
    logo = FilerImageField(verbose_name=u'Логотип', null=True, blank=True)
    desc = RichTextField(u'Описание', max_length=4096, blank=True)
    services = models.ManyToManyField(Service, null=True, blank=True,
                                verbose_name=Service._meta.verbose_name_plural)
    tariffs = models.ManyToManyField(Tariff, null=True, blank=True,
                                verbose_name=Tariff._meta.verbose_name_plural)
    specials = models.ManyToManyField(Special, null=True, blank=True,
                                verbose_name=Special._meta.verbose_name_plural)
    sorting = models.PositiveIntegerField('Сортировка', default=0)
    rating = models.CharField(u'Рейтинг', blank=True, max_length=255)

    class Meta:
        ordering= 'sorting', 'pk',
        verbose_name=u'Провайдер'
        verbose_name_plural=u'Провайдеры'

    @property
    def calculate_rating(self):
        default = '-'
        if self.review_set.exists():
            return self.review_set.actual(
                                ).aggregate(models.Avg('rate')
                                ).get('rate__avg', default)
        return default

    def get_absolute_url(self):
        return reverse('providers:prvdrs_list')+'?pk='+str(self.pk)

    def thumbnail_logo(self, size=None):
        if self.logo and size:
            opts = dict(size=size, crop=True)
            try:
                thumbnail = get_thumbnailer(self.logo).get_thumbnail(opts)
                return thumbnail.url
            except: pass

    @property
    def adaptive_logo_url(self):
        return self.thumbnail_logo(size=(218,145)) or ''

    @property
    def logo_120_url(self):
        return self.thumbnail_logo(size=(120,120)) or ''