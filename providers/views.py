#coding: utf-8
from django.db.models import Q
from django.views.generic import ListView

from .models import Provider, Special


class ProviderList(ListView):
    model = Provider
    template_name = 'providers.html'

    def get_queryset(self):
        qs = super(ProviderList, self).get_queryset()
        #TODO: refact this
        q=Q()
        if self.request.GET:
            body = self.request.GET
            if body.get('pk'):
                q&=Q(pk=body.get('pk'))
        return qs.filter(q)
prvdrs_list = ProviderList.as_view()


class Specials(ListView):
    model = Special
    template_name = 'specials.html'
    queryset = Special.objects.actual_specials()

    def get_queryset(self):
        qs = super(Specials, self).get_queryset()
        #TODO: refact this
        if self.request.GET:
            if self.request.GET.get('pk'):
                pk = int(self.request.GET.get('pk'))
                if pk == 1:
                    return qs.exclude(services__in=(2,3)) #inet
                elif pk==2:
                    return qs.filter(services=2).exclude(services__in=(3,)) #inet+tv
                elif pk==3:
                    return qs.filter(services=3) # inet+tv+phone
        return qs
specials = Specials.as_view()