#coding: utf-8
from rest_framework import serializers

from .models import Provider, Tariff, Service


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service


class TariffSerializer(serializers.ModelSerializer):
    services = ServiceSerializer()
    class Meta:
        model = Tariff
        fields = 'id', 'desc', 'price', 'title', 'speed', 'services'


class ProviderSerializer(serializers.ModelSerializer):
    url = serializers.Field(source="get_absolute_url")
    logo_120_url = serializers.Field(source="logo_120_url")
    #tariffs = TariffSerializer()
    #tv_tariffs = TariffSerializer(Tariff.objects.internet_tv(), many=True)
    #phone_tariffs = TariffSerializer(Tariff.objects.internet_phone_tv(), many=True)

    class Meta:
        model = Provider

    def to_native(self, obj):
        data = super(ProviderSerializer, self).to_native(obj)
        data['inet_trfs'] = TariffSerializer(
                                            obj.tariffs.internet(),
                                            many=True
                                        ).data
        data['tv_trfs'] = TariffSerializer( obj.tariffs.internet_tv(), 
                                            many=True
                                        ).data
        data['phone_trfs'] = TariffSerializer(
                                            obj.tariffs.internet_phone_tv(),
                                            many=True
                                        ).data
        return data