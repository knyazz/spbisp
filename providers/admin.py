#coding: utf-8
from django.contrib import admin

from base.admin import BaseAdminwithOrder

from .models import Provider, Tariff, Service, Special, ITStandart

class TariffAdmin(BaseAdminwithOrder):
    list_display = ('id', 'title', 'price', 'speed', 'stp')
    list_display_links = ('id', 'title')

    def stp(self, obj):
        return u', '.join(obj.services.values_list('title', flat=True))
    stp.short_description = u'Услуги'
admin.site.register(Tariff,TariffAdmin)

admin.site.register(Provider,BaseAdminwithOrder)
admin.site.register(Service,BaseAdminwithOrder)
admin.site.register(Special,BaseAdminwithOrder)
admin.site.register(ITStandart,BaseAdminwithOrder)