# coding: utf-8
from django.db import models


class ReviewsQuerySet(models.query.QuerySet):
    def actual(self):
        q = models.Q(moderation=True)
        return self.filter(q)


class ReviewsManager(models.Manager):
    use_for_related_fields = True
    def get_queryset(self):
        return ReviewsQuerySet(self.model, using=self._db)

    def actual(self):
        return self.get_queryset().actual()