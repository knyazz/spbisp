#coding: utf-8
from django.db import models

from providers.models import Provider

from .managers import ReviewsManager


class Review(models.Model):
    objects=ReviewsManager()
    name = models.CharField(u'Имя', max_length=255)
    text = models.TextField(u'Текст сообщения', max_length=4096)
    rate = models.PositiveSmallIntegerField(u'Оценка', default=5)
    moderation = models.BooleanField('Опубликовать', default=False)
    added = models.DateTimeField(auto_now_add=True)
    provider = models.ForeignKey(Provider,
                                verbose_name=Provider._meta.verbose_name)
    __unicode__ = lambda self: self.name
    class Meta:
        ordering = '-added', 'moderation', 'provider'
        verbose_name=u'Отзыв'
        verbose_name_plural=u'Отзывы'