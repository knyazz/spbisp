from django.conf.urls import patterns, url

urlpatterns = patterns('reviews.views',
    url(r'^review_create/$', 'review_create', name='review_create'),
    url(r'^reviews_list/$', 'reviews_list', name='reviews_list'),
)