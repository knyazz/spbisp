#coding: utf-8
from django.contrib import admin

from base.admin import BaseAdmin

from .models import Review


admin.site.register(Review, BaseAdmin)