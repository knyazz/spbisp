#coding: utf-8
from django.db.models import Q
from django.views.generic import ListView

from rest_framework import generics

from .forms import ReviewForm
from .models import Review


class OrderCreate(generics.CreateAPIView):
    model = Review
review_create = OrderCreate.as_view()


class RewiesList(ListView):
    model = Review
    queryset = model.objects.filter(moderation=True)
    template_name = 'reviews.html'

    def get_context_data(self, **kwargs):
        ctx = super(RewiesList, self).get_context_data(**kwargs)
        ctx['review_form'] = ReviewForm()
        return ctx

    def get_queryset(self):
        qs = super(RewiesList, self).get_queryset()
        #TODO: refact this
        q=Q()
        if self.request.GET:
            body = self.request.GET
            if body.get('provider'):
                q&=Q(provider=body.get('provider'))
        return qs.filter(q)
reviews_list = RewiesList.as_view()