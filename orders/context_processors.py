from .forms import OrderForm

def content(request):
    return {
            'order_form': OrderForm(),
    }