#coding: utf-8
from django.db import models

from address.models import Address
from providers.models import Tariff, Provider


class Order(models.Model):
    address = models.ForeignKey(Address,verbose_name=Address._meta.verbose_name)
    provider = models.ForeignKey(Provider, 
                                verbose_name=Provider._meta.verbose_name)
    tariff = models.ForeignKey(Tariff, verbose_name=Tariff._meta.verbose_name)
    apartment = models.PositiveIntegerField(u'Квартира')
    fio = models.CharField(u'ФИО', max_length=1024)
    phone = models.CharField(u'Контактный телефон', max_length=32)
    time = models.DateTimeField(u'Дата и время звонка для связи')
    is_checked = models.BooleanField(u'Обработана', default=False)

    class Meta:
        verbose_name=u'Заявка на подключение'
        verbose_name_plural = u'Заявки на подключение'