#coding: utf-8
from rest_framework import generics

from .models import Order


class OrderCreate(generics.CreateAPIView):
    model = Order
order_create = OrderCreate.as_view()