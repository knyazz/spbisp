#coding: utf-8
from django.contrib import admin

from base.admin import BaseAdmin

from .models import Order


class OrderAdmin(BaseAdmin):
    readonly_fields = ('address', 'provider', 'tariff',)
    list_filter = ('fio', 'phone',)
    list_display_links = ('id',)+list_filter
    list_display = list_display_links+('address',)
admin.site.register(Order, OrderAdmin)