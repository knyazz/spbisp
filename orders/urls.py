from django.conf.urls import patterns, url


urlpatterns = patterns('orders.views',
    url(r'^order_create/$', 'order_create', name='order_create'),
)
